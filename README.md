# geramundo
A stochastic, procedural platformer-game map generator.

The goal is to produce maps with a Terraria-like look and feel.

The tool is coded in C++ and is cross-platform. It uses mostly the standard library, but uses code of the following libraries:

* [TCLAP](http://tclap.sourceforge.net/)
* [RapidXML](http://rapidxml.sourceforge.net/)
* [EasyBMP](http://easybmp.sourceforge.net/) (included as git submodule)
* (Optional) [SDL 1.2](https://www.libsdl.org/index.php), for a preview functionality

Althrough the tool is functional, it's still in beta stage, so expect lots of bugs. Currently it works best with default options.

The code is in a eclipse project so its needed to have either Eclipse for C/C++ Developers or an Eclipse with the CDT plugin installed to open the project.
