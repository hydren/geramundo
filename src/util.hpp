/*
 * util.hpp
 *
 *  Created on: 26/02/2014
 *      Author: felipe
 */

#ifndef UTIL_HPP_
#define UTIL_HPP_

#define null NULL

#include <iostream>
#include <cstdlib>
#include <sstream>
#include <vector>
#include <string>
#include <ctime>

#include "libgeramun/util.hpp"

// Todo: replace this code with 'futil'

using std::cout;
using std::endl;
using std::vector;
using std::string;

/** Prints the given grid */
inline void printGrid(Grid grid)
{
	for(unsigned i=0; i<grid.size(); i++)
	{
		for(unsigned j=0; j<grid[i].size(); j++)
		{
			cout << grid[i][j];
		}
		cout << endl;
	}
}

template <typename Type>
vector< vector <Type> > transpose(const vector< vector<Type> >& matrix)
{
	//if empty, return a new empty
	if(matrix.size() == 0)
		return vector< vector<int> >();

	//safety check
	for(unsigned i = 0, size=matrix[0].size(); i < matrix.size(); i++)
		if(matrix[i].size() != size)
			throw string("Matrix with differing row sizes! " + matrix[i].size());

	vector< vector<Type> > matrix_t(matrix[0].size(), vector<Type>(matrix.size()));

	for(unsigned i = 0; i < matrix.size(); i++)
		for(unsigned j = 0; j < matrix[i].size(); j++)
			matrix_t[j][i] = matrix[i][j];

	return matrix_t;
}

#endif /* UTIL_HPP_ */
