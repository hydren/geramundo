/*
 * screen.hpp
 *
 *  Created on: May 4, 2014
 *      Author: faguiar
 */

#ifndef SCREEN_HPP_
#define SCREEN_HPP_

#include "util.hpp"

// uncomment to disable show mode (and remove SDL dependency)
//#define NO_SDL

/** Controls whether to show preview window or not */
extern bool PREVIEW_WINDOW;

/** Controls whether the window should run automatically, or should step each iteration with user confirmation */
extern bool PREVIEW_WINDOW_AUTO;

/** Specify preview window zoom. DO NOT USE ZERO! */
extern unsigned PREVIEW_WINDOW_ZOOM;

/** Specify the delay of each iteration.
 *
 * When > 0, specify how much time (in ms) to wait between iterations.
 * When < 0, specify how much iterations it should skip showing (frame skip).
 *
 * Ex: 500 - waits 500ms between iterations
 *      -3 - shows a iteration each 3 iterations (and doesn't delay)
 *       0 - doesn't delay
 *      -1 - doesn't skip iteration (and doesn't delay)
*/
extern int PREVIEW_WINDOW_DELAY;

void create_screen(int larg, int altu);

void show_grid(const Grid& grid);

void show_grid_and_wait(const Grid& grid);


#endif /* SCREEN_HPP_ */
