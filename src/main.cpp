//============================================================================
// Name        : main.cpp
// Author      : Carlos Faruolo and Felipe Aguiar
//============================================================================

#include "util.hpp"

#include "parser_bmp.hpp"
#include "parser_txt.hpp"
#include "parser_xml.hpp"
#include "transform_parsing.hpp"
#include "screen.hpp"

#include "libgeramun/version.h"
#include "libgeramun/generators.hpp"
#include "libgeramun/preview_callback.hpp"

#include <tclap/CmdLine.h>

#include <iostream>
#include <string>

#include <cstdio>

#define GERAMUNDO_VERSION LIBGERAMUN_VERSION_STR
#define PROGRAM_PRESENTATION_TITLE "geramundo - a stochastic, procedural 2D platformer map generator"

using std::cout;
using std::endl;
using std::string;
using TCLAP::CmdLine;
using TCLAP::ValueArg;
using TCLAP::MultiArg;
using TCLAP::SwitchArg;
using TCLAP::UnlabeledMultiArg;

extern unsigned SPRING_MAX_ITERATIONS;
extern bool LIBGERAMUN_DEBUG;

int main(int argc, char** argv)
{
	SPRING_MAX_ITERATIONS = 500000;
	srand(time(null));
	preview_grid_callback = show_grid_and_wait;
	try {
		//configure arguments parser
		CmdLine cmd(PROGRAM_PRESENTATION_TITLE, ' ', GERAMUNDO_VERSION, true);

		//trick to modify the output from --version
		struct MyOutput : public TCLAP::StdOutput { virtual void version(TCLAP::CmdLineInterface& c) {
		cout << "\n " PROGRAM_PRESENTATION_TITLE " - version " GERAMUNDO_VERSION "\n"<< endl;
		}} my_output; cmd.setOutput(&my_output);

		//=== window arguments
		SwitchArg switch_show_window("S","show-window","Shows a preview window while the program is running. Default is false.", false);
		SwitchArg switch_manual_window("M","window-manual","Makes the preview window wait for a manual confirmation (press Return/Enter) each program iteration. Default is false.", false);
		ValueArg<unsigned> arg_window_zoom("Z","window-zoom","Defines a zoom factor for the preview window. Default is 1 (no zoom).", false, 1, "unsigned");
		ValueArg<int> arg_window_delay("D","window-delay","Delays each program iteration (good for better visualization) by a given time (ms). Negative values makes the preview window skips iterations (shows faster). Default is -50.", false, -50, "int");

		cmd.add(arg_window_delay);
		cmd.add(arg_window_zoom);
		cmd.add(switch_manual_window);
		cmd.add(switch_show_window);

		//=== general arguments
		SwitchArg switch_debug("d", "debug", "Toggle printing debug messages along the execution.", false);
		cmd.add(switch_debug);

		ValueArg<string> arg_output_raw_file("r", "raw-output", "Outputs the generated map to a plain text file with the given filename. The result should be a matrix of integers.", false, "", "string");
		cmd.add( arg_output_raw_file );

		ValueArg<string> arg_output_tmx_file("x", "tmx-output", "Outputs the generated map to a TMX file with the given filename.", false, "", "string");
		cmd.add( arg_output_tmx_file );

		ValueArg<string> arg_output_bmp_file("b", "bmp-output", "Outputs the generated map to a BMP image file with the given filename.", false, "", "string");
		cmd.add( arg_output_bmp_file );

		SwitchArg switch_empty("e", "empty", "Toggle the creation of a empty map", false);
		cmd.add(switch_empty);

		MultiArg<string> arg_input_files("i", "input", "Optional argument to specify script files with transforms to be applied on the map.", false, "string");
		cmd.add( arg_input_files);

		ValueArg<unsigned> arg_width("c", "columns", "Width (in blocks) of the resulting map. This is the same as specifing the number of columns of the resulting matrix.", true, 0, "unsigned");
		cmd.add( arg_width );
		ValueArg<unsigned> arg_height("l", "lines", "Height (in blocks) of the resulting map. This is the same as specifing the number of lines of the resulting matrix.", true, 0, "unsigned");
		cmd.add( arg_height );

		//=== transformation arguments
		UnlabeledMultiArg<string> multi("transforms", "A set of valid transforms in the program, with its parameters in order, to be applied on the matrix", false, "transforms");
		cmd.add( multi );

		//parse
		cmd.parse( argc, argv );

		//greeter :)
		cout << "\n" PROGRAM_PRESENTATION_TITLE " - version " GERAMUNDO_VERSION << endl;

		// load the arguments values

		LIBGERAMUN_DEBUG = switch_debug.getValue();

		unsigned real_width = arg_width.getValue() , real_height=arg_height.getValue();

		if(switch_show_window.getValue()==true) //use preview window
		{
			cout << "loading preview window..."; cout.flush();
			PREVIEW_WINDOW=true;
			PREVIEW_WINDOW_AUTO = not switch_manual_window.getValue();
			PREVIEW_WINDOW_ZOOM = arg_window_zoom.getValue();
			PREVIEW_WINDOW_DELAY = arg_window_delay.getValue();

			if(PREVIEW_WINDOW) create_screen(real_width, real_height);
			cout << "done" << endl;
		}

		cout << "creating matrix..."; cout.flush();
		Grid grid = createGrid(real_width, real_height);
		cout << "done" << endl;

		if(switch_empty.getValue()==false)
		{
			cout << "embedded transforms..."; cout.flush();
			generator5(grid);
		}

		vector<string> trans_args_files = arg_input_files.getValue();
		if(arg_input_files.isSet())
		{
			for(unsigned i = 0; i < trans_args_files.size(); i++)
			{
				vector<string> trans_args = parseTransformFile(trans_args_files[0]);
				cout << "\ntransforms (from \"" << trans_args_files[0] << "\"):" << endl;
				parseTransforms(trans_args, grid);
				trans_args.clear();
			}
			cout << endl;
		}

		if(multi.isSet())
		{
			cout << "\ntransforms:" << endl;
			vector<string> trans_args = multi.getValue();
			parseTransforms(trans_args, grid);
		}

		if(arg_output_bmp_file.isSet())
		{
			cout << "saving result to bmp output..."; cout.flush();
			saveToBMP(grid, arg_output_bmp_file.getValue());
			cout << "done" << endl;
		}

		if(arg_output_tmx_file.isSet())
		{
			cout << "saving result to tmx output..."; cout.flush();
			saveToXML(grid, arg_output_tmx_file.getValue());
			cout << "done" << endl;
		}

		if(arg_output_raw_file.isSet())
		{
			cout << "saving result to plain text output..."; cout.flush();
			saveToTXT(grid, arg_output_raw_file.getValue());
			cout << "done" << endl;
		}

	//printGrid(grid);

	} catch (TCLAP::ArgException &e)
	{ std::cerr << "error: " << e.error() << " no arg " << e.argId() << std::endl; }

	return EXIT_SUCCESS;
}
