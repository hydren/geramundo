/*
 * parser_xml.cpp
 *
 *  Created on: 18/05/2014
 *      Author: felipe
 */

#include "parser_xml.hpp"

#include <sstream>
#include <string>
#include <iostream>
#include <cstdlib>

#include <rapidxml/rapidxml.hpp>
#include <rapidxml/rapidxml_print.hpp>
#include <rapidxml/rapidxml_utils.hpp>

using std::stringstream;
using std::string;
using std::vector;

rapidxml::file<> *file;
rapidxml::xml_document<> doc;
rapidxml::xml_node<> *current;

//////////////// prototypes
void initXml();
void appendNode(const string name, const string text="");
void addAttribute(const string attr, const string value);
void setCurrentNode(rapidxml::xml_node<> *node);
void returnToParent();
void generateTileNodes(std::vector< std::vector<int> > v);

rapidxml::xml_node<>* getNode(const string name, rapidxml::xml_node<>* parentNode = NULL);
string getAttribute(const string s, rapidxml::xml_node<>* node);
vector< vector<int> > generateGrid(const string width, const string heigth, rapidxml::xml_node<>* n);

string convertToString(int value);

////////////////semi-legacy

struct tileset
{
	string tilesetName;
	string imageSource;
	int imageWidth, imageHeight;
};

struct mapinfo
{
	int mapWidth, mapHeight;

	int tileWidth, tileHeight;

	int layerWidth, layerHeight;
	string layerName;

	vector< vector<int> > map;

	vector<tileset> tileSets;

	mapinfo(const Grid& grid)
	: mapWidth(grid[0].size()), mapHeight(grid.size()), tileWidth(16), tileHeight(16)
	{
		map = transpose(grid);

		setTileSet("tileset1", "dirt.png", 16, 16);
		setTileSet("tileset2", "stone.png", 16, 16);
		setTileSet("tileset3", "water.png", 16, 16);
		setTileSet("tileset4", "grass.png", 16, 16);

		layerName = "Map";
		layerWidth = mapWidth;
		layerHeight = mapHeight;
	}

	void setTileSet(string tilesetName, string imageSource, int imageWidth, int imageHeight)
	{
		tileset t;
		t.tilesetName = tilesetName;
		t.imageSource = imageSource;
		t.imageWidth = imageWidth;
		t.imageHeight = imageHeight;

		tileSets.push_back(t);
	}
};


////////////////legacy

bool readMapFromFile(const std::string path) {
	file = new rapidxml::file<>(path.c_str());

	try {
		cout << "lendo..." << endl;
		doc.parse<0>( (char*)file->data() );
	}catch (rapidxml::parse_error & e) {
		cout << "Erro: " << e.what() << endl;
		return false;
	}
	return true;

}

bool saveMapToFile(const std::string path) {
	std::string s;
	print(std::back_inserter(s), doc, 0);

	std::ofstream file;
	file.open(path.c_str());
	file << doc;


	return false;
}

void createXMLFromMap(mapinfo* m)
{
	initXml(); //create declaration and node map

	addAttribute("width", convertToString(m->mapWidth) );
	addAttribute("height", convertToString(m->mapHeight) );
	addAttribute("tilewidth", convertToString(m->tileWidth) );
	addAttribute("tileheight", convertToString(m->tileHeight) );

	int gidCounter = 1;
	for(unsigned int i=0; i < m->tileSets.size(); i++) {

		appendNode("tileset");
		addAttribute("firstgid", convertToString(gidCounter));
		addAttribute("name", m->tileSets[i].tilesetName);
		addAttribute("tilewidth", convertToString(m->tileWidth) );
		addAttribute("tileheight", convertToString(m->tileHeight) );

		appendNode("image");
		addAttribute("source", m->tileSets[i].imageSource);
		addAttribute("width", convertToString(m->tileSets[i].imageWidth) );
		addAttribute("height", convertToString(m->tileSets[i].imageHeight) );

		returnToParent(); //tileset
		returnToParent(); //map
		int numberOfTiles = ( m->tileSets[i].imageWidth / m->tileWidth ) * ( m->tileSets[i].imageHeight / m->tileHeight );
		gidCounter += numberOfTiles;
	}


		appendNode("layer");
		addAttribute("name", m->layerName);
		addAttribute("width", convertToString(m->layerWidth) );
		addAttribute("height", convertToString(m->layerHeight) );

			appendNode("data");
				generateTileNodes(m->map);
}

vector< vector<int> > createMapFromXML(const string path) {
	if(!readMapFromFile(path)) {
		cout << "Error reading file " << path << endl;
		exit(EXIT_FAILURE);
	}
	rapidxml::xml_node<>* nodeMap = getNode("map");
	if (nodeMap == NULL) {
		cout << "Error reading map node." << endl;
		exit(EXIT_FAILURE);
	}
	string w = getAttribute("width", nodeMap);
	string h = getAttribute("height", nodeMap);

	rapidxml::xml_node<>* nodeLayer = getNode("layer", nodeMap);

	return generateGrid(w, h, getNode("data", nodeLayer));
}

void appendNode(const std::string name, const std::string text) {
	char *_name = doc.allocate_string(name.c_str());
	char *_text = doc.allocate_string(text.c_str());

	rapidxml::xml_node<> *node = doc.allocate_node(rapidxml::node_element , _name, _text);

	current->append_node(node);

	setCurrentNode(node);
}

void addAttribute(const std::string name, const std::string value) {
	char *_name = doc.allocate_string(name.c_str());
	char *_value = doc.allocate_string(value.c_str());

	rapidxml::xml_attribute<> *attr = doc.allocate_attribute(_name, _value);

	current->append_attribute(attr);
}

void initXml() {
	rapidxml::xml_node<> *declaration = doc.allocate_node(rapidxml::node_declaration);
	declaration->append_attribute(doc.allocate_attribute("version", "1.0"));
	declaration->append_attribute(doc.allocate_attribute("encoding", "UTF-8"));

	doc.append_node(declaration);

	current = doc.allocate_node(rapidxml::node_element, "map");
	current->append_attribute(doc.allocate_attribute("version", "1.0"));
	current->append_attribute(doc.allocate_attribute("orientation", "orthogonal"));

	doc.append_node(current);
}

void setCurrentNode(rapidxml::xml_node<> *node) {
	current = node;
}

void returnToParent() {
	current = current->parent();
}

void generateTileNodes(std::vector< std::vector<int> > v) {
	for( int i=0; (unsigned int)i<v[0].size(); i++ )
	{
		for( int j=0; (unsigned int)j<v.size(); j++ )
		{
			appendNode("tile");
			addAttribute("gid", convertToString(v[j][i]));
			returnToParent();
		}
	}
}

rapidxml::xml_node<>* getNode(const string name, rapidxml::xml_node<>* parentNode) {
	return parentNode==NULL ? doc.first_node(name.c_str()) : parentNode->first_node(name.c_str());
}

string getAttribute(const string s, rapidxml::xml_node<>* node) {
	rapidxml::xml_attribute<> *attr = node->first_attribute(s.c_str());

//	cout << "attribute: " << attr->name() << " value: " << attr->value() << endl;

	return attr->value();
}

vector<vector<int> > generateGrid(const string width, const string height, rapidxml::xml_node<>* n) {
	int w = atoi(width.c_str());
	int h = atoi(height.c_str());
	int count=0;

	vector< vector<int> > grid;
	vector<int> row;

	for (rapidxml::xml_node<> *tile = n->first_node("tile"); tile; tile = tile->next_sibling()) {
		row.push_back( atoi(tile->first_attribute("gid")->value()) );
		if (++count == h) {
			grid.push_back(row);
			count = 0;
			row.clear();
		}
	}
	if(grid.size() < (unsigned)w) {
		cout << "Error while constructing the map." << endl;
		exit(EXIT_FAILURE);
	}
	return grid;
}

string convertToString(int value) {
	stringstream ss;
	ss << value;
	return ss.str();
}

///////////////////////// principal //////////////////////////
void saveToXML(const Grid& grid, const string& filepath)
{
	mapinfo* m = new mapinfo(grid);
	createXMLFromMap(m);
	saveMapToFile(filepath);
}

Grid loadFromXML(const string& filepath)
{
	return createMapFromXML(filepath);
}



