/*
 * parser_bmp.cpp
 *
 *  Created on: 20/05/2014
 *      Author: felipe
 */

#include "parser_bmp.hpp"

#include "EasyBMP/EasyBMP.h"

using std::string;

static RGBApixel createRGBApixel(ebmpBYTE r, ebmpBYTE g, ebmpBYTE b, ebmpBYTE a=0)
{
	const RGBApixel tmp = { b, g, r, a };
	return tmp;
}

/** Saves the grid in BMP image format */
void saveToBMP(const Grid& grid, const string& filepath)
{
	BMP image;
	RGBApixel empty = 	createRGBApixel(255,255,255),
			  ground = 	createRGBApixel(150,100, 50),
			  rock = 	createRGBApixel(100,100,100),
			  water = 	createRGBApixel(0  ,150,255),
			  grass = 	createRGBApixel(150,255,  0);

	image.SetSize(grid[0].size(), grid.size());

	for(unsigned i=0; i<grid.size(); i++)
	{
		for(unsigned j=0; j<grid[i].size(); j++)
		{
			switch (grid[i][j])
			{
				case 0:
					image.SetPixel(j, i, empty);
					break;
				case 1:
					image.SetPixel(j, i, ground);
					break;
				case 2:
					image.SetPixel(j, i, rock);
					break;
				case 3:
					image.SetPixel(j, i, water);
					break;
				case 4:
					image.SetPixel(j, i, grass);
					break;
				default:
					image.SetPixel(j, i, empty);
					break;
			}
		}
	}
	image.WriteToFile(filepath.c_str());
}

