/*
 * parser_bmp.hpp
 *
 *  Created on: 20/05/2014
 *      Author: felipe
 */

#ifndef PARSER_BMP_HPP_
#define PARSER_BMP_HPP_

#include "util.hpp"

/** Saves the grid in BMP image format */
void saveToBMP(const Grid& map, const std::string& filepath);

//ToDo
Grid loadFromBMP(const std::string& filepath);


#endif /* PARSER_BMP_HPP_ */
