/*
 * parser_txt.cpp
 *
 *  Created on: 19/06/2014
 *      Author: professor
 */

#include "parser_txt.hpp"

#include <fstream>

using std::string;

void saveToTXT(const Grid& grid, const string& filepath)
{
	std::ofstream file;

	file.open(filepath.c_str());

	for(unsigned i=0; i<grid.size(); i++)
	{
		for(unsigned j=0; j<grid[i].size(); j++)
		{
			file << grid[i][j];
		}
		file << endl;
	}
	file.close();
}


