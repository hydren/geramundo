/*
 * transform_parsing.hpp
 *
 *  Created on: 20/06/2014
 *      Author: felipe
 */

#ifndef TRANSFORM_PARSING_HPP_
#define TRANSFORM_PARSING_HPP_

#include "util.hpp"

/** Parse args and execute transform functions accordingly. */
void parseTransforms(const std::vector<std::string>& trans_args, Grid& grid);

/** Parse transform arguments from file with the specified filename.  */
std::vector<std::string> parseTransformFile(const std::string& filename);

#endif /* TRANSFORM_PARSING_HPP_ */
