/*
 * parser_xml.hpp
 *
 *  Created on: 18/05/2014
 *      Author: felipe
 */

#ifndef PARSER_XML_HPP_
#define PARSER_XML_HPP_

#include "util.hpp"

void saveToXML(const Grid& map, const std::string& filepath);
Grid loadFromXML(const std::string& filepath);

#endif /* PARSER_XML_HPP_ */
