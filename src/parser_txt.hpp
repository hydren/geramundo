/*
 * parser_txt.hpp
 *
 *  Created on: 19/06/2014
 *      Author: professor
 */

#ifndef PARSER_TXT_HPP_
#define PARSER_TXT_HPP_

#include "util.hpp"

void saveToTXT(const Grid& grid, const std::string& filepath);

#endif /* PARSER_TXT_HPP_ */
