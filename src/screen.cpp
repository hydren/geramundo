/*
 * screen.cpp
 *
 *  Created on: May 4, 2014
 *      Author: faguiar
 */

//versao com SDL, funcionando
#include "screen.hpp"

/** Controls whether to show preview window or not */
bool PREVIEW_WINDOW=false;

/** Controls whether the window should run automatically, or should step each iteration with user confirmation */
bool PREVIEW_WINDOW_AUTO=false;

/** Specify preview window zoom. DO NOT USE ZERO! */
unsigned PREVIEW_WINDOW_ZOOM=1;

/** Specify the delay of each iteration.
 * 
 * When > 0, specify how much time (in ms) to wait between iterations.
 * When < 0, specify how much iterations it should skip showing (frame skip).
 * 
 * Ex: 500 - waits 500ms between iterations
 *      -3 - shows a iteration each 3 iterations (and doesn't delay)
 *       0 - doesn't delay
 *      -1 - doesn't skip iteration (and doesn't delay)
*/
int PREVIEW_WINDOW_DELAY=1;
short delay_skip_aux = 0;

#ifndef NO_SDL

#include <SDL/SDL.h>

SDL_Surface* screen;
SDL_Event temp_event;

void create_screen(int larg, int altu)
{
	if( SDL_Init(SDL_INIT_VIDEO)== -1)
	{ //inicializar a SDL
		cout << "error on SDL_Init:" << SDL_GetError() << endl;
	}
	else
	{
		screen = SDL_SetVideoMode(larg*PREVIEW_WINDOW_ZOOM, altu*PREVIEW_WINDOW_ZOOM, 32, SDL_SWSURFACE | SDL_DOUBLEBUF);
		SDL_WM_SetCaption("Preview window", null);
	}
}

void show_grid(const Grid& grid)
{
	if(screen != NULL)
	{
		Uint32 empty_color = SDL_MapRGB(screen->format, 255, 255, 255);
		Uint32 ground_color = SDL_MapRGB(screen->format, 150, 100, 50);
		Uint32 rock_color = SDL_MapRGB(screen->format, 100, 100, 100);
		Uint32 water_color = SDL_MapRGB(screen->format, 0, 150, 255);
		Uint32 grass_color = SDL_MapRGB(screen->format, 150, 255, 0);

		for(unsigned i=0; i<grid.size(); i++)
		{
			for(unsigned j=0; j<grid[i].size(); j++)
			{
				SDL_Rect where;
				where.x = j*PREVIEW_WINDOW_ZOOM;
				where.y = i*PREVIEW_WINDOW_ZOOM;
				where.w = PREVIEW_WINDOW_ZOOM;
				where.h = PREVIEW_WINDOW_ZOOM;

				switch (grid[i][j])
				{
					case 0:
						SDL_FillRect(screen, &where, empty_color);
						break;
					case 1:
						SDL_FillRect(screen, &where, ground_color);
						break;
					case 2:
						SDL_FillRect(screen, &where, rock_color);
						break;
					case 3:
						SDL_FillRect(screen, &where, water_color);
						break;
					case 4:
						SDL_FillRect(screen, &where, grass_color);
						break;
					default:
						SDL_FillRect(screen, &where, empty_color);
						break;
				}
			}
		}

		SDL_Flip(screen);
	}
}

void show_grid_and_wait(const Grid& grid)
{
	if(not PREVIEW_WINDOW or screen == null)
		return;

	if(not PREVIEW_WINDOW_AUTO)
	{
		show_grid(grid);

		//wait
		bool pass=false;
		while(not pass)
		{
			while(SDL_PollEvent(&temp_event))
			{
				switch(temp_event.type)
				{
					case SDL_QUIT:
						exit(EXIT_SUCCESS);
						break;
					case SDL_KEYDOWN:
						if(not PREVIEW_WINDOW_AUTO)
						if(temp_event.key.keysym.sym == SDLK_RETURN or temp_event.key.keysym.sym == SDLK_g or temp_event.key.keysym.sym == SDLK_h or temp_event.key.keysym.sym == SDLK_j)
							pass=true;
						break;
				}
			}
		}
	}
	else
	{
		if(PREVIEW_WINDOW_DELAY < 0)
		{
			if((++delay_skip_aux)%(-PREVIEW_WINDOW_DELAY)==0)
				show_grid(grid);
		}
		else
		{
			show_grid(grid);
			SDL_Delay(PREVIEW_WINDOW_DELAY);
		}

		while(SDL_PollEvent(&temp_event))
		{
			switch(temp_event.type)
			{
				case SDL_QUIT:
					exit(EXIT_SUCCESS);
					break;
				default:
					break;
			}
		}
	}
}

#endif

//versao sem SDL
#ifdef NO_SDL

#include "screen.hpp"

void create_screen(int larg, int altu)
{}

void show_grid(const Grid& grid)
{}

void show_grid_and_wait(const Grid& grid)
{}

#endif

