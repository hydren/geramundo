/*
 * transform_parsing.cpp
 *
 *  Created on: 20/06/2014
 *      Author: felipe
 */

#include "transform_parsing.hpp"

#include "libgeramun/cave.hpp"
#include "libgeramun/midpoint_displacement.hpp"
#include "libgeramun/miners.hpp"
#include "libgeramun/random_walk.hpp"
#include "libgeramun/spring.hpp"
#include "libgeramun/surf_replacer.hpp"

#include <fstream>

#include <cstdlib>
#include <cerrno>
#include <climits>

static inline
long parseInteger(const string& str)
{
	return strtol(str.c_str(), null, 0);
}

static inline
unsigned long parseUnsigned(const string& str)
{
	return strtoul(str.c_str(), null, 0);
}

static inline
double parseDecimal(const string& str)
{
	return strtod(str.c_str(), null);
}

static inline
unsigned long parseableUnsigned(const string& str)
{
	errno = 0;
	char* temp;
	const unsigned int val = strtoul(str.c_str(), &temp, 0);
	return not (temp == str or *temp != '\0' or ((val == LONG_MIN or val == LONG_MAX) and errno == ERANGE));
}

/** Parse args and execute transform functions accordingly. */
void parseTransforms(const vector<string>& trans_args, Grid& grid)
{
	for(unsigned i = 0; i < trans_args.size(); i++)
	{
		const string& s = trans_args[i];
		if(s=="fill")
		{
			if(i+2 > trans_args.size()-1)
			{
				cout << "missing arguments for "+s+"! ignoring..." << endl;
				continue;
			}
			int flag = parseInteger(trans_args[i+1]);
			float portion = parseDecimal(trans_args[i+2]);
			cout << s << " " << flag << " " << portion << endl;
			fillPartially(grid, flag, portion);
			i += 2;//skip used args
		}
		else if(s=="mdisp")
		{
			if(i+2 > trans_args.size()-1)
			{
				cout << "missing arguments for "+s+"! ignoring..." << endl;
				continue;
			}
			int flag = parseInteger(trans_args[i+1]);
			float factor = parseDecimal(trans_args[i+2]);
			cout << s << " " << flag << " " << factor << endl;
			midpoint_displacement(grid, flag, factor);
			i += 2;
		}
		else if(s=="rb_mdisp")
		{
			if(i+2 > trans_args.size()-1)
			{
				cout << "missing arguments for "+s+"! ignoring..." << endl;
				continue;
			}
			int flag = parseInteger(trans_args[i+1]);
			float factor = parseDecimal(trans_args[i+2]);
			cout << s << " " << flag << " " << factor << endl;

			//map borders randomly defined
			unsigned height_left = (4*grid.size())/5;
			unsigned height_right = (4*grid.size())/5;
			for(unsigned k = 0; k < grid.size(); k++)
			{
				if(k < height_left)
					grid[k][0] = 0;
				else
					grid[k][0] = 1;

				if(k < height_right)
					grid[k][grid[k].size()-1] = 0;
				else
					grid[k][grid[k].size()-1] = 1;
			}

			midpoint_displacement(grid, flag, factor);
			i += 2;
		}
		else if(s=="miners")
		{
			if(i+4 > trans_args.size()-1)
			{
				cout << "missing arguments for "+s+"! ignoring..." << endl;
				continue;
			}
			int flag = parseInteger(trans_args[i+1]);
			int bgflag = parseInteger(trans_args[i+2]);
			unsigned n_miners = parseUnsigned(trans_args[i+3]);
			int spawnProb = parseInteger(trans_args[i+4]);
			cout << s << " " << flag << " " << bgflag  << " " << n_miners  << " " << spawnProb << endl;
			miners_algorithm2(grid, flag, bgflag, n_miners, spawnProb);
			i += 4;
		}
		else if(s=="r4walk")
		{
			if(i+6 > trans_args.size()-1)
			{
				cout << "missing arguments for "+s+"! ignoring..." << endl;
				continue;
			}
			int flag = parseInteger(trans_args[i+1]);
			unsigned row = parseUnsigned(trans_args[i+2]);
			unsigned col = parseUnsigned(trans_args[i+3]);
			unsigned len = parseUnsigned(trans_args[i+4]);
			float switchProb = parseDecimal(trans_args[i+5]);
			int brushSize = parseInteger(trans_args[i+6]);

			cout << s << " " << flag << " " << row  << " " << col  << " " << len <<  " " << switchProb <<  " " << brushSize << endl;
			random_walk_4dir(grid, flag, row, col, len, switchProb, brushSize);
			i += 6;
		}
		else if(s=="r8walk")
		{
			if(i+6 > trans_args.size()-1)
			{
				cout << "missing arguments for "+s+"! ignoring..." << endl;
				continue;
			}
			int flag = parseInteger(trans_args[i+1]);
			unsigned row = parseUnsigned(trans_args[i+2]);
			unsigned col = parseUnsigned(trans_args[i+3]);
			unsigned len = parseUnsigned(trans_args[i+4]);
			float switchProb = parseDecimal(trans_args[i+5]);
			int brushSize = parseInteger(trans_args[i+6]);

			cout << s << " " << flag << " " << row  << " " << col  << " " << len <<  " " << switchProb <<  " " << brushSize << endl;
			random_walk_8dir(grid, flag, row, col, len, switchProb, brushSize);
			i += 6;
		}
		else if(s=="cave")
		{
			if(i+7 > trans_args.size()-1)
			{
				cout << "missing arguments for "+s+"! ignoring..." << endl;
				continue;
			}
			int flag = parseInteger(trans_args[i+1]);
			unsigned row = parseUnsigned(trans_args[i+2]);
			unsigned col = parseUnsigned(trans_args[i+3]);
			unsigned len = parseUnsigned(trans_args[i+4]);
			float switchProb = parseDecimal(trans_args[i+5]);
			int brushSize = parseInteger(trans_args[i+6]);
			float brushSizeVar = parseDecimal(trans_args[i+7]);

			cout << s << " " << flag << " " << row  << " " << col  << " " << len <<  " " << switchProb <<  " " << brushSize << " " << brushSizeVar << endl;
			cave(grid, flag, row, col, len, switchProb, brushSize, brushSizeVar, NORTH);
			i += 7;
		}
		else if(s=="spring")
		{
			if(i+4 > trans_args.size()-1)
			{
				cout << "missing arguments for "+s+"! ignoring..." << endl;
				continue;
			}
			int flag = parseInteger(trans_args[i+1]);
			unsigned volume = parseUnsigned(trans_args[i+2]);
			unsigned row = parseUnsigned(trans_args[i+3]);
			unsigned col = parseUnsigned(trans_args[i+4]);
			cout << s << " " << flag << " " << volume << " " << row  << " " << col << endl;
			spring(grid, flag, volume, row, col);
			i += 4;
		}
		else if(s=="springn")
		{
			if(i+5 > trans_args.size()-1)
			{
				cout << "missing arguments for "+s+"! ignoring..." << endl;
				continue;
			}
			int flag = parseInteger(trans_args[i+1]);
			unsigned volume = parseUnsigned(trans_args[i+2]);
			float dropProb = parseDecimal(trans_args[i+3]);
			unsigned row1 = parseUnsigned(trans_args[i+4]);
			unsigned col1 = parseUnsigned(trans_args[i+5]);
			cout << s << " " << flag << " " << volume << " " << dropProb << " " << row1  << " " << col1;
			i += 5;

			vector<GridRegion> coords;
			coords.push_back(GridRegion(col1, row1));


			while( i+2 < trans_args.size() //there are 2 (or more) arguments left
				and parseableUnsigned(trans_args[i+1]) //both arguments are unsigned
				and parseableUnsigned(trans_args[i+2]))//both arguments are unsigned
			{
				int rown = parseUnsigned(trans_args[i+1]);
				int coln = parseUnsigned(trans_args[i+2]);
				coords.push_back(GridRegion(coln, rown)); //add coordinates
				i += 2;
				cout  << " " << rown  << " " << coln;
			}
			cout << endl;
			springn(grid, flag, volume, coords, dropProb);
		}
		else if(s=="surfrep")
		{
			if(i+2 > trans_args.size()-1)
			{
				cout << "missing arguments for "+s+"! ignoring..." << endl;
				continue;
			}
			int flag = parseInteger(trans_args[i+1]);
			int replacing_flag = parseInteger(trans_args[i+2]);
			cout << s << " " << flag << " " << replacing_flag << endl;
			surface_replace(grid, flag, replacing_flag);
			i += 2;
		}
		else
		{
			cout << "ignoring argument " << s << endl;
		}
	}
}

/** Parse transform arguments from file with the specified filename.  */
vector<string> parseTransformFile(const string& filename)
{
	vector<string> trans_args;
	std::ifstream stream;
	stream.open(filename.c_str());
	string str;

	while(stream.good())
	{
		char c = stream.get();

		if(c == '#') do c = stream.get(); while(c != '\n' and stream.good());

		if(c == ' ' or c == '\n')
		{
			if(str == "") continue;
			trans_args.push_back(str);
			str.clear();
		}
		else if(stream.good())
			str += c;

		if(not stream.good())
		{
			if(str == "") continue;
			trans_args.push_back(str);
			str.clear();
		}
	}
	return trans_args;
}

