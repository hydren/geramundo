#!/bin/bash

width=1024
height=256

if [ -e "_script.txt" ]
then
  rm _script.txt
fi

echo "rb_mdisp 1 0.8" >> _script.txt
echo "miners 2 1 200 0.5" >> _script.txt

for(( i = 1 ; i <= 16 ; ++i )); do
	echo "r4walk 2 192 $(( (i*1024)/17 )) 200 0.75 1" >> _script.txt
done

#for(( i = 1 ; i <= 16 ; ++i )); do
#	echo "r8walk 0 192 $(( (i*1024)/17 )) 200 0.1 2" >> _script.txt
#done

for(( i = 1 ; i <= 16 ; ++i )); do
	echo "cave 0 192 $(( (i*1024)/17 )) 200 0.3 2 0.5" >> _script.txt
done

echo "spring 3 2500 0 1" >> _script.txt
echo "spring 3 2500 0 1023" >> _script.txt

echo "springn 3 1000 0.5 0 32 0 792 0 64 0 512 0 128 0 256 0 275" >> _script.txt

echo "surfrep 1 4" >> _script.txt

echo "_script.txt :"

cat _script.txt

./geramundo -c $width -l $height -b teste.bmp -i _script.txt -S
